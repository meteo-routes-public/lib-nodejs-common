const logLevels = {'ERROR': 1, 'WARN': 2, 'INFO': 3, 'DEBUG': 4}

const json = log => {
	const logLevel = logLevels[process.env.LOG_LEVEL] || 3
	if(logLevels[log.type] > logLevel) return
	log.dt = new Date()
	console.log(JSON.stringify(log))
}

const error = (msg, ctx=null) => json({
	type: 'ERROR', msg, ctx
})

const dumpErr = async (msg, err, ctx=null) => {
	error(msg, {error: err.message || err, stack: err.stack && err.stack.split('\n').map(l => l.trim()).join(', '), ...ctx })
}

const warn = (msg, ctx=null) => json({
	type: 'WARN', msg, ctx
})
const info = (msg, ctx=null) => json({
	type: 'INFO', msg, ctx
})
const debug = (msg, ctx=null) => json({
	type: 'DEBUG', msg, ctx
})

module.exports = {
    json, warn, error, dumpErr, info, debug
}
