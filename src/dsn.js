const protocols = {};

protocols["redis"] = (dsn, options = { queueName: true }) => {
	const matches = dsn.match(/^redis(s)?:\/\/(.*):(.*)@(.*):(\d+)\/?(.*)$/);
	if (!matches) {
		throw new Error("Bad format for redis dsn. Expected redis(s)://user:password@host:port(/queue)");
	}
	let [__, tlsSuffix, username, password, host, port, queue] = matches;
	if (options.queueName && queue === '') {
		throw new Error("Invalid redis dsn, queue name required");
	}
	return {
		tls: tlsSuffix == "s" ? {} : null,
		username: username || null,
		password,
		host,
		port,
		queue: queue || null,
	};
};

protocols["ftp"] = (dsn) => {
	let matches = dsn.match(/(ftp|ftps):\/\/([^:]+):([^@]+)@([^:]+):(\d+)/);
	if (!matches) {
		throw new Error("Bad DSN parameter. Expected ftp://user:pass@host:port");
	}
	for (let idx of Object.keys(matches)) {
		matches[idx] = decodeURIComponent(matches[idx]);
	}
	let [_, protocol, user, password, host, port] = matches;
	let secure = protocol == "ftps";
	return { host, port, user, password, secure };
};

protocols["s3"] = (dsn) => {
	let matches = dsn.match(/^s3:\/\/(.*):(.*)@(.*):(.*)\/(.*)/);
	if (!matches) {
		throw new Error("Bad format for aws dsn. Expected s3://KEY:SECRET@ENDPOINT:REGION/BUCKET");
	}
	let [__, accessKeyId, secretAccessKey, endpoint, region, bucket] = matches;
	const params = { accessKeyId, secretAccessKey, region, bucket };
	if (endpoint) params["endpoint"] = endpoint;
	return params;
};

protocols["mysql"] = (dsn) => {
	let matches = dsn.match(/^mysql:\/\/(.*):(.*)@(.*):(\d+)\/(.*)/);
	if (!matches) {
		throw new Error("Bad format for db dsn. Expected mysql://user:password@host:port/dbname");
	}
	let [__, user, password, host, port, database] = matches;
	return { host, port, user, password, database };
};

module.exports = (protocol, dsn, options) => {
	protocol = protocol.toLowerCase();
	if (protocols[protocol]) return protocols[protocol](dsn, options);
	throw new Error(`Protocol unknown : ${protocol}`);
};
