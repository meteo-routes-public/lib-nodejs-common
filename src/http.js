const request = (url, options = {}, data = null) => {
	return new Promise((resolve, reject) => {
		let chunks = [];
		const protocol = url.startsWith("http://") ? "http" : "https";
		const httpCli = require(protocol);

		const req = httpCli.request(url, options, (res) => {
			const { statusCode, headers } = res;
			res.on("data", (chunk) => chunks.push(chunk));
			res.on("end", () => {
				const buffer = Buffer.concat(chunks);
				resolve({ data: buffer, statusCode, headers })
			});
		});

		req.on("error", reject);

		if (data) {
			req.write(data);
		}

		req.end();
	});
};

const get = (url, opts) => request(url, opts);
const post = (url, data, opts) => request(url, { method: "POST", ...opts }, data);

module.exports = {
	get,
	post,
	request
};
