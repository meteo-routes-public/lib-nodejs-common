const tap = require("tap");
const nock = require("nock");
const { get, post } = require("../src/http");

tap.test("should fetch data from http url", async (t) => {
	nock("http://example.com").get("/data").reply(200, "Hello, world!");

	const result = await get("http://example.com/data");
	t.equal(result, "Hello, world!");
	t.end();
});

tap.test("should fetch data from https url", async (t) => {
	nock("https://example.com").get("/data").reply(200, "Hello, HTTPS world!");

	const result = await get("https://example.com/data");
	t.equal(result, "Hello, HTTPS world!");
	t.end();
});

tap.test("should handle http errors", async (t) => {
	nock("http://example.com").get("/error").replyWithError("Something went wrong");

	try {
		await get("http://example.com/error");
	} catch (e) {
		t.equal(e.message, "Something went wrong");
	}
	t.end();
});

tap.test("should post data to http url", async (t) => {
	nock("http://example.com")
			.post("/data", { key: "value" }) // attend une requête POST avec ces données
			.reply(200, "Posted successfully");

	const result = await post("http://example.com/data", { key: "value" });
	t.equal(result, "Posted successfully");
	t.end();
});

tap.test("should handle post http errors", async (t) => {
	nock("http://example.com")
			.post("/error")
			.replyWithError("Something went wrong with POST");

	try {
			await post("http://example.com/error", { key: "value" });
	} catch (e) {
			t.equal(e.message, "Something went wrong with POST");
	}
	t.end();
});