const tap = require("tap");
const dsn = require("../src/dsn");

// Tests pour Redis
tap.test("Redis : should parse valid DSN", async (t) => {
	const result = dsn("redis", "redis://user:password@host:1234/0");
	t.same(result, {
		tls: null,
		username: "user",
		password: "password",
		host: "host",
		port: "1234",
		queue: "0",
	});
	t.end();
});

tap.test("Redis : should throw error when queue name is not provided", (t) => {
	t.throws(
			() => {
					dsn("redis", "redis://user:password@host:1234");
			},
			new Error("Invalid redis dsn, queue name required")
	);
	t.end();
});

tap.test("Redis : should parse valid DSN with Redis SSL", async (t) => {
	const result = dsn("redis", "rediss://user:password@host:1234/0");
	t.same(result, {
		tls: {},
		username: "user",
		password: "password",
		host: "host",
		port: "1234",
		queue: "0",
	});
	t.end();
});

tap.test("Redis : should handle DSN without queue", async (t) => {
	const result = dsn("redis", "redis://user:password@host:1234", { queueName: false });
	t.same(result, {
		tls: null,
		username: "user",
		password: "password",
		host: "host",
		port: "1234",
		queue: null,
	});
	t.end();
});

tap.test("Redis : should handle DSN without username", async (t) => {
	const result = dsn("redis", "redis://:password@host:1234/0");
	t.same(result, {
		tls: null,
		username: null,
		password: "password",
		host: "host",
		port: "1234",
		queue: "0",
	});
	t.end();
});

tap.test("Redis : should throw error for invalid DSN", async (t) => {
	t.throws(() => {
		dsn("redis", "invalid-dsn");
	}, new Error("Bad format for redis dsn. Expected redis(s)://user:password@host:port(/queue)"));
	t.end();
});

// Tests pour FTP
tap.test("FTP : should parse valid DSN", async (t) => {
	const result = dsn("ftp", "ftp://user:password@host:21");
	t.same(result, {
		host: "host",
		port: "21",
		user: "user",
		password: "password",
		secure: false,
	});
	t.end();
});

tap.test("FTP : should throw error for invalid DSN", async (t) => {
	t.throws(() => {
		dsn("ftp", "invalid-dsn");
	}, new Error("Bad DSN parameter. Expected ftp://user:pass@host:port"));
	t.end();
});

// Tests pour S3
tap.test("S3 : should parse valid DSN", async (t) => {
	const result = dsn("s3", "s3://KEY:SECRET@ENDPOINT:REGION/BUCKET");
	t.same(result, {
		accessKeyId: "KEY",
		secretAccessKey: "SECRET",
		endpoint: "ENDPOINT",
		region: "REGION",
		bucket: "BUCKET",
	});
	t.end();
});

tap.test("S3 : should throw error for invalid DSN", async (t) => {
	t.throws(() => {
		dsn("s3", "invalid-dsn");
	}, new Error("Bad format for aws dsn. Expected s3://KEY:SECRET@ENDPOINT:REGION/BUCKET"));
	t.end();
});

tap.test("S3 : should handle DSN without endpoint", async (t) => {
	const result = dsn("s3", "s3://KEY:SECRET@:REGION/BUCKET");
	t.same(result, {
		accessKeyId: "KEY",
		secretAccessKey: "SECRET",
		region: "REGION",
		bucket: "BUCKET",
	});
	t.end();
});

// Tests pour MySQL
tap.test("MySQL : should parse valid DSN", async (t) => {
	const result = dsn("mysql", "mysql://user:password@host:3306/dbname");
	t.same(result, {
		host: "host",
		port: "3306",
		user: "user",
		password: "password",
		database: "dbname",
	});
	t.end();
});

tap.test("MySQL : should throw error for invalid DSN", async (t) => {
	t.throws(() => {
		dsn("mysql", "invalid-dsn");
	}, new Error("Bad format for db dsn. Expected mysql://user:password@host:port/dbname"));
	t.end();
});

// Default
tap.test("Unknow protocol", async (t) => {
	t.throws(() => {
		dsn("dummy", "invalid-dsn");
	}, "`Protocol unknown : dummy");
	t.end();
});
