const tap = require('tap');
const { error, warn, info, debug, dumpErr } = require('../src/log');

tap.test('should log ERROR', t => {
    let output = null;
    console.log = (msg) => { output = JSON.parse(msg); };
    error('Test error');
    t.equal(output.type, 'ERROR');
    t.equal(output.msg, 'Test error');
    t.end();
});

tap.test('should log WARN', t => {
    let output = null;
    console.log = (msg) => { output = JSON.parse(msg); };
    warn('Test warning');
    t.equal(output.type, 'WARN');
    t.equal(output.msg, 'Test warning');
    t.end();
});

tap.test('should log INFO', t => {
    let output = null;
    console.log = (msg) => { output = JSON.parse(msg); };
    info('Test info');
    t.equal(output.type, 'INFO');
    t.equal(output.msg, 'Test info');
    t.end();
});

tap.test('should not log DEBUG if level is below', t => {
	process.env.LOG_LEVEL = 'INFO';
	let output = null;
	console.log = (msg) => { output = JSON.parse(msg); };
	debug('Should not log this');
	t.equal(output, null);
	t.end();
});

tap.test('should log DEBUG if level is DEBUG', t => {
	process.env.LOG_LEVEL = 'DEBUG';
	let output = null;
	console.log = (msg) => { output = JSON.parse(msg); };
	debug('Test debug');
	t.equal(output.type, 'DEBUG');
	t.equal(output.msg, 'Test debug');
	t.end();
});

tap.test('should log ERROR with error details', t => {
	process.env.LOG_LEVEL = 'ERROR';
	let output = null;
	console.log = (msg) => { output = JSON.parse(msg); };

	const fakeError = new Error('Test error message');
	dumpErr('Test error', fakeError);

	t.equal(output.type, 'ERROR');
	t.equal(output.msg, 'Test error');
	t.ok(output.ctx.error.includes('Test error message'));
	t.ok(output.ctx.stack);

	t.end();
});

tap.test('should log ERROR with error details even if error has no message', t => {
	process.env.LOG_LEVEL = 'ERROR';
	let output = null;
	console.log = (msg) => { output = JSON.parse(msg); };

	const fakeError = new Error();
	dumpErr('Test error', fakeError);

	t.equal(output.type, 'ERROR');
	t.equal(output.msg, 'Test error');
	t.ok(JSON.stringify(output.ctx.error) === JSON.stringify(fakeError)); // Pas de message d'erreur
	t.ok(output.ctx.stack); // La pile d'appels doit toujours être là

	t.end();
});

// Rétablir les valeurs par défaut
process.env.LOG_LEVEL = undefined;
console.log = console._log || console.log;
